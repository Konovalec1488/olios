function menu(){
    $( ".cross" ).hide();
    $( ".vtoggle" ).hide();
    $( ".hamburger" ).click(function() {
        $( ".vtoggle" ).slideToggle( "slow", function() {
            $( ".hamburger" ).hide();
            $( ".cross" ).show();
        });
    });
    $( ".cross" ).click(function() {
        $( ".vtoggle" ).slideToggle( "slow", function() {
            $( ".cross" ).hide();
            $( ".hamburger" ).show();
        });
    });
}

function mobiMenu(){
    $( "#mobi_sidebar" ).click(function() {
        $( "#mobi_menu" ).slideToggle( "slow", function() {
        });
    });
}

function slider(){
    $('input[type=radio]').change(function() {

    	if(this.value==1){
    		$("main").animate({opacity:0.0},400);
            setTimeout(function(){
                $("main").css({"background":"url(img/main-bg1.jpg) center center no-repeat", "background-size":"cover"});
             }, 400);                
            $("main").animate({opacity:1},700);
    	}
    	else if(this.value==2){
            $("main").animate({opacity:0.0},400);
            setTimeout(function(){
                $("main").css({"background":"url(img/main-bg2.jpg) center center no-repeat", "background-size":"cover"});
             }, 400);                
    		$("main").animate({opacity:1},700);
    	}
    	else if(this.value==3){
    		$("main").animate({opacity:0.0},400);
            setTimeout(function(){
                $("main").css({"background":"url(img/main-bg3.jpg) center center no-repeat", "background-size":"cover"});
             }, 400);               
            $("main").animate({opacity:1},700);
    	}
    	else if(this.value==4){
    		$("main").animate({opacity:0.0},400);
            setTimeout(function(){
                $("main").css({"background":"url(img/main-bg4.jpg) center center no-repeat", "background-size":"cover"});
             }, 400);                
            $("main").animate({opacity:1},700);
    	}
    });
}

function likes(){
    $( ".likes" ).click(function() {
        var html = $('.num').text();
        html=parseInt(html); 
        html+=1;
        $('.num').text(html);
    });
}

function zoom(){
        var num = $("#foto").css("width");
        num = parseInt(num);
        var num_max=num*1.17;  /* +17% */
    $( ".plus" ).click(function() {
        $(".minus").css({"fill":"#0023ff", "cursor":"pointer"});
        var plus = $("#foto").css("width");
        var paddingT = $(".photo_products").css("padding-top");
        var paddingB = $(".photo_products").css("padding-bottom");
        plus = parseInt(plus);
        paddingT = parseInt(paddingT);
        paddingB = parseInt(paddingB);
        if(plus<=num_max){
            plus+=50;
            paddingT-=25;
            paddingB-=25;
            $("#foto").css("width", plus);
            $(".photo_products").css("padding-top", paddingT);
            $(".photo_products").css("padding-bottom", paddingB);
        }else{
            $(".plus").css({"fill":"#dbdbdb", "cursor":"default"});
        }
    });
    $( ".minus" ).click(function() {
        $(".plus").css({"fill":"#0023ff", "cursor":"pointer"});
        var minus = $("#foto").css("width");
        var paddingT = $(".photo_products").css("padding-top");
        var paddingB = $(".photo_products").css("padding-bottom");
        minus=parseInt(minus);
        paddingT = parseInt(paddingT);
        paddingB = parseInt(paddingB);
        if(minus>num){
            minus-=50;
            paddingT+=25;
            paddingB+=25;
            $("#foto").css("width", minus);
            $(".photo_products").css("padding-top", paddingT);
            $(".photo_products").css("padding-bottom", paddingB);
        }else{
            $(".minus").css({"fill":"#dbdbdb", "cursor":"default"});
        }
    });

}

$(document).ready(function () {  
    menu();
    slider();
    likes();
    zoom();
    mobiMenu();
});